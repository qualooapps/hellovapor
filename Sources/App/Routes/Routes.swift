import Vapor

extension Droplet {
    func setupRoutes() throws {
        get("hello") { req in
            var json = JSON()
            try json.set("hello", "world")
            return json
        }

        get("plaintext") { req in
            return "Hello, world!"
        }

        // response to requests to /info domain
        // with a description of the request
        get("info") { req in
            return req.description
        }

        get("description") { req in return req.description }
        
        get("hello", "vapor") { req in
            return "Hello Vapor"
        }
        
        get("hello", String.parameter) { req -> String in
            let name = try req.parameters.next(String.self)
            return "Hello \(name)!"
        }
        
        try resource("posts", PostController.self)
    }
}
